CREATE TABLE clients(
    c_id int(3) PRIMARY KEY AUTO_INCREMENT,
    c_name varchar(20) NOT NULL,
    c_mail varchar(20) UNIQUE,
    c_balance int(10) NOT NULL
    );
    
INSERT INTO `clients`(`c_id`, `c_name`, `c_mail`, `c_balance`) VALUES 
	(1, 'rohan', 'apc@gmail.com', 15000),
    (2, 'rahul	', 'def@gmail.com', 40000),
    (3, 'rohit', 'hij@gmail.com', 6000),
    (4, 'sumit', 'klm@gmail.com', 7000),
    (5, 'mohan', 'mno@gmail.com', 10000),
    (6, 'sonam', 'pqr@gmail.com', 16000),
    (7, 'saurya', 'stu@gmail.com', 800),
    (8, 'vinod', 'vxy@gmail.com', 20000),
    (9, 'ankit', 'xyz@gmail.com', 100),
    (10, 'surendra', 'pop@gmail.com', 120)
    ;

CREATE TABLE transaction (
  sr_no int(3) PRIMARY KEY AUTO_INCREMENT,
  sender text NOT NULL,
  receiver text NOT NULL,
  balance int(10) NOT NULL,
  date_time datetime NOT NULL DEFAULT current_timestamp()
);


SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";

COMMIT



/* 	INSERT INTO `users` (`id`, `name`, `email`, `balance`) 
	VALUES (1, 'rohan', 'apc@gmail.com', 15000);
	 (2, 'rahul	', 'def@gmail.com	', 40000);
	 (3, 'rohit', 'hij@gmail.com', 6000);
	 (4, 'sumit', 'klm@gmail.com', 7000);
	 (5, 'mohan', 'mno@gmail.com', 10000);
	 (6, 'sonam', 'pqr@gmail.com', 16000);
	 (7, 'saurya', 'stu@gmail.com', 800);
	 (8, 'vinod', 'vxy@gmail.com', 20000);
	 (9, 'ankit', 'xyz@gmail.com', 100);
     (10, 'surendra', 'pop@gmail.com', 120); */